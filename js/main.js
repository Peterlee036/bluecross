$(function(){
    $('[data-toggle="tooltip"]').tooltip();

    
    // step 2-2 Same as Applicant 與投保人為同一人: 隱藏/開啟投保人欄位
    $("#differentAsApplicant").hide();
    $("input[name='radioSameAsApplicant']").on('change',function(){
        var radioSelect = $(this).val();
        switch (radioSelect) {
            case 'option1':
                $("#differentAsApplicant").hide(500);
                break;
            case 'option2':
                $("#differentAsApplicant").show(500);
                break;
            default:
                $("#differentAsApplicant").hide();
                break;
        }
    })

    // secondBeneficiary 候補受益人欄位 隱藏/開啟
    $("#secondBeneficiary").hide();
    $("#checkBeneficiarySecond").on('change',function(){
        if($(this).prop('checked')){
            $("#secondBeneficiary").show(500)
        }else{
            $("#secondBeneficiary").hide(500)
        }
    })

    // step 3 繳費模式選擇
    $("#pmPPFSelect").hide();
    $("#pModeSelect").on('change',function(){
        $("#pmPPFSelect").hide(500);
        $("#pMethodSelect").children().remove();
        var option
        var optionObj = [
            {
                id: "1",
                text: 'Direct Billing 直接結算'
            },
            {
                id: "2",
                text: 'Autopay by Bank Account 銀行自動轉帳'
            },
            {
                id: "3",
                text: 'Autopay by Credit Card 信用卡自動轉帳'
            },
            {
                id: "4",
                text: 'Premium Prepayment Fund 預繳保費'
            }
        ]
        var select = $(this).val();
        switch (select) {
            case '1':
                optionObj.forEach(function(e){
                    if( e.id == 2 ){
                        option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                    }else{
                        option = '<option value='+e.id+'>'+e.text+'</option>'
                    }
                    $('#pMethodSelect').append(option)
                });
                $("#pMethodSelect").val(null).trigger('change')
                break;
            case '2':
                optionObj.forEach(function(e){
                    if( e.id == 4 ){
                        option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                    }else{
                        option = '<option value='+e.id+'>'+e.text+'</option>'
                    }
                    $('#pMethodSelect').append(option)
                });
                $("#pMethodSelect").val(null).trigger('change')
                break;
            case '3':
                optionObj.forEach(function(e){
                    if( e.id == 1 || e.id == 4){
                        option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                    }else{
                        option = '<option value='+e.id+'>'+e.text+'</option>'
                    }
                    $('#pMethodSelect').append(option)
                });
                $("#pMethodSelect").val(null).trigger('change')
                break; 
            case '4':
                optionObj.forEach(function(e){
                    if( e.id == 4 ){
                        option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                    }else{
                        option = '<option value='+e.id+'>'+e.text+'</option>'
                    }
                    $('#pMethodSelect').append(option)
                });
                $("#pMethodSelect").val(null).trigger('change')
                break;        
            default:
                // console.log('沒選')
                break;
        }
    })
    $("#pMethodSelect").on('change',function(){
        $("#pmPPFSelect").hide(500);
        var select = $(this).val();
        switch (select) {
            case '4':
                $("#pmPPFSelect").show(500)
                break;
        
            default:
                break;
        }
        console.log($(this).val())
    })
    // 首期繳費款項支付方式 連動inital account 選單
    $("input[name='radioInitPaid']").on('change',function(){
        $("#accountTypeSelect-init").children().remove();
        var option
        var optionObj = [
            {
                id: "1",
                text: 'HKD Bank Account 港元銀行戶口'
            },
            {
                id: "2",
                text: 'USD BEA Bank Account 東亞銀行美元戶口'
            },
            {
                id: "3",
                text: 'CNY Bank Account 人民幣戶口'
            },
            {
                id: "4",
                text: 'BEA Credit Card 東亞銀行信用卡'
            }
        ]
        var select = $(this).val();
        if(  select == '1' || select == '2' || select == '4'){
            optionObj.forEach(function(e){
                if( e.id == 4 ){
                    option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                }else{
                    option = '<option value='+e.id+'>'+e.text+'</option>'
                }
                $('#accountTypeSelect-init').append(option)
            });
            $("#accountTypeSelect-init").val(null).trigger('change')
        }else {
            optionObj.forEach(function(e){
                if( e.id == 1 ||  e.id == 2 || e.id == 3){
                    option = '<option value='+e.id+' disabled>'+e.text+'</option>'
                }else{
                    option = '<option value='+e.id+'>'+e.text+'</option>'
                }
                $('#accountTypeSelect-init').append(option)
            });
            $("#accountTypeSelect-init").val(null).trigger('change')
        }
    })
    //step 3 首期付款 戶口帳號 銀行/信用卡
    $("#creditSelection-init").hide();
    $("#bankSelection-init").hide();
    $("#accountTypeSelect-init").on('change',function(){
        $("#creditSelection-init").hide(500);
        $("#bankSelection-init").hide(500);
        var select = $(this).val();
        if( select == '1' || select == '2' || select == '3'){
            $("#bankSelection-init").show(500);
        }else if( select == '4'){
            $("#creditSelection-init").show(500);
        }
    })

    //step 3 續期付款 戶口帳號 銀行/信用卡
    $("#creditSelection-regular").hide();
    $("#bankSelection-regular").hide();
    $("#accountTypeSelect-regular").on('change',function(){
        $("#creditSelection-regular").hide(500);
        $("#bankSelection-regular").hide(500);
        var select = $(this).val();
        if( select == '1' || select == '2' || select == '3'){
            $("#bankSelection-regular").show(500);
        }else if( select == '4'){
            $("#creditSelection-regular").show(500);
        }
    })


    // option 
    var age = $("#age-select");
    var age2 = $("#age-select-2");
    var option ; 
    for( var i=0; i<=70; i++ ){
        option = '<option value='+i+'>'+i+'</option>'
        age.append(option)
        age2.append(option)
    }
   
    

    //select 2
    $('select').each(function () {
        var selectLength = $(this).data('select-length');
        $(this).select2({
            theme: 'bootstrap4',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow_clear')),
            minimumResultsForSearch: -1,
            maximumSelectionLength: selectLength == undefined ? 0 : selectLength, 
        });
    });

    // Beneficiary step table add row
    $(".addTDRow").on('click',function(){
        var tr = $(this).parents('tr');
        var cloneTR = tr.clone(true);
        var countNO =  $(this).parents('tbody').find('tr').length + 1
        if(countNO < 6 ){
            cloneTR.find('.removeTDRow').show()
            if( countNO == 5 ){
                cloneTR.find('.addTDRow').hide()
            }
            
            cloneTR.children('th').text(countNO)
            cloneTR.children('td').find('input').val('')
            $(this).parents('tbody').append(cloneTR)
            // $(this).parent().empty()
            $(this).parent().children().hide()
        }
    }) 
    // Beneficiary step table remove row
    $(".removeTDRow").on('click',function(){
        var thisTR = $(this).parents('tr');
        var preTR = thisTR.prev();
        var countNO =  $(this).parents('tbody').find('tr').length - 1
        thisTR.remove();
        preTR.find('.optionRow').children().show();
        if(countNO == 1){
            preTR.find('.optionRow').children('.removeTDRow').hide()
        }
    })


    // login
    $("button.login-confirm").on('click',function(){
        var id = $("#login-input").val().trim();
        switch (id) {
            case 'admin':
                location.href = "table.html";
                break;  
            case 'sales':
                location.href = "table2.html";
                break;

            default:
                var div = document.createElement("div");
                div.innerHTML = "<div>管理者: admin</div><div>業務: sales</div>";
                swal({
                    className: "customer-swal",
                    title: "CAUTION",
                    content: div ,
                });
                break;
        }
        // location.href = "index.html";
    })
    // logout
    $(".logout-confirm").on('click',function(){
        location.href = "index.html";
    })

    $(".formList").on('click',function(){
        location.href = "step.html";
    })

    $(".menuList").on('click',function(e){
        e.preventDefault();
        location.href = "table2.html";
    })


    collapse();
    stepCollapse();

    $(".dropdown-item").on('click',function(){
        var dropdownSelect = $(this).parents('.dropdown').find('button.dropdown-toggle');
        dropdownSelect.siblings('.dropdown-menu').find('.dropdown-item').removeClass('active');
        $(this).addClass('active');
    });

    //標單驗證
    $("input[data-valid]").on('input',function(){
        var $valid = $(this).data('valid') // 驗證類別
        var chackName = 'check'+$valid
        var result = universal[chackName]($(this).val().trim());
        if(result){
            $(this).siblings("i.material-icons").text("check_circle");
            $(this).siblings("i.material-icons").removeClass('text-danger');
            $(this).siblings("i.material-icons").addClass('active text-success'); 
        }else{
            $(this).siblings("i.material-icons").text("error");
            $(this).siblings("i.material-icons").removeClass('text-success');
            $(this).siblings("i.material-icons").addClass('active text-danger');
        }
        errorInfo($(this),result);
    });

    $(".swal-active").on('click',function(e){
        e.preventDefault();
        swalUse();
    })

    

});

// collapse function
function collapse(){
    $('.collapse').on('show.bs.collapse',function(){   
        var collapseSelect = $(this).parents('.accordion');
        collapseSelect.find('button.btn-link').attr('data-toggle','collapse');
        $(this).parents('.card').find('button.btn-link').attr('data-toggle',null);
        stepProccess(collapseSelect,$(this));
    }) 
}
function stepCollapse(){
    $(".circle").on('click',function(){
        var currentID =  $(this).parent().attr('class');
        if(!currentID.match('active')){
            $("#"+currentID).collapse('show');
        }
    })
}



// step-proccess
function stepProccess(area,target){
    area.siblings('.step-process').find('[class^="step-process-"]').removeClass('active');
    var currentID = target.attr('id');
    $("."+currentID).addClass('active');
}

//驗證
(function(obj){
    typeof module === 'object' && module.exports ? module.exports = obj : this[obj.name] = obj
})({
    name: 'universal', // 前端 <script> 標籤的全域名稱
    // checkEmail: val => /.+@.+\..+/.test(val),
    checkPassword: function (val) {
        return /.{8,}/.test(this._password = val)
    },
    checkConfirmPassword: function (val) {
        return this._password === val
    },
    checkPhone: function (val) {
        return /^\d{5,}$/.test(val)
    },
    checkString: function (val) {
        return /^\w{2,}/.test(val)
    }
    // checkAddress: val => /[\u4e00-\u9fa5]/.test(val),
    // checkImage: ary => ary.length <= 3 && ary.every(file => file.width <= 150 || file.height <= 150),
    // checkCardNumber (val) {
    //     val = val.replace(/\s/g, '')
    //     return (/^4[0-9]{12}(?:[0-9]{3})?$/.test(val) && 'visa') ||
    //     (/^5[1-5][0-9]{14}$/.test(val) && 'master') || ''
    // }
})

// 驗證 info
function errorInfo(val,status){
    if(status){
        val.siblings('.error-info').hide();
    }else{
        val.siblings('.error-info').show();
    }
    
}

// swal
function swalUse(){
    swal({
        className: "customer-swal",
        title: "CAUTION",
        text: "For required fields, please fill in the correct information" ,
    });
}