$(function(){

    // Q2 
    $("#initPremiumSelect").on('change',function(){
        var select = $(this).val();
        if(select == 3){
            $("#initPremiumSelectOtherHide").hide(500)
        }else{
            $("#initPremiumSelectOtherHide").show(500)
        }
    }) 

    // Q3 
    $("input[name='initPremiumRadio']").on('change',function(){
        var select = $(this).val();
        if(select == 2){
            $('#initPremiumYesShow').hide(500)
        }else{
            $('#initPremiumYesShow').show(500)
        }
    })

    // Q5
    $("#supPremiumSelect").on('change',function(){
        var select = $(this).val();
        if(select == 1){
            $("#supPremiumSelectSelf").show(500)
        }else{
            $("#supPremiumSelectSelf").hide(500)
        }
    })

    // Q6 
    $("input[name='occupationRadio']").on('change',function(){
        var select = $(this).val();
        if(select == 2){
            $("#occupationRadioNoShow").show(500)
        }else{
            $("#occupationRadioNoShow").hide(500)
        }
    })

    // 核准人員 第二位
    $('#checkApprovalSecond').on('change',function(){
        if($(this).prop('checked')){
            $("#checkApprovalSecondShow").show(500)
        }else{
            $("#checkApprovalSecondShow").hide(500)
        } 
    })
})